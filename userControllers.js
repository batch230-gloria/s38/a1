const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
	
	})

	return newUser.save().then((user, error)=>{

		if(error){
			return false;
		}
		else{
			return true;
		}

	})	
}


// function checkEmailExist{}
module.exports.checkEmailExist = (reqBody) =>{
	return User.find({email: reqBody.email}).then(result =>{

		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); // return true or false
			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return{access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;
			}
		}
	})
}


// [ACTIVITY - s38]

module.exports.getProfile = (reqBody) => {

	return User.findById({_id: reqBody.id}).then((result,err) => {

		if(err){
			return false;
		}
		else{
			result.password = "******";
			return result.save()
		}
		
	})

}